<?php
//index.php
include('dbConnection.php');
//include('function.php');

if(!isset($_SESSION["type"]))
{
	header("location:login.php");
}

include('header.php');
require_once 'functionDashboard.php';

?>
<br />
<div class="row">
	<?php
	if($_SESSION['type'] == 'master')
	{
		?>
		<div class="col-md-3">
			<div class="panel panel-default">
				<div class="panel-heading"><strong>Hoggannada</strong></div>
				<div class="panel-body" align="center">
					<a href="Hoggaannada.php"><h1><?php echo getHoggaanno(); ?></h1></a>
				</div>
			</div>
		</div>
		<div class="col-md-3">
			<div class="panel panel-default">
				<div class="panel-heading"><strong>Qeybaha</strong></div>
				<div class="panel-body" align="center">
					<h1><?php echo "6"; ?></h1>
				</div>
			</div>
		</div>
		<div class="col-md-3">
			<div class="panel panel-default">
				<div class="panel-heading"><strong>Ururada Madaxa banaan</strong></div>
				<div class="panel-body" align="center">
					<h1><?php echo "10"; ?></h1>
				</div>
			</div>
		</div>
		<div class="col-md-3">
			<div class="panel panel-default">
				<div class="panel-heading"><strong>Colalka Madaxa banaan</strong></div>
				<div class="panel-body" align="center">
					<h1><?php echo "10"; ?></h1>
				</div>
			</div>
		</div>
		<div class="col-md-3">
			<div class="panel panel-default">
				<div class="panel-heading"><strong>Naafada</strong></div>
				<div class="panel-body" align="center">
					<h1><?php echo "100"; ?></h1>
				</div>
			</div>
		</div>
		<div class="col-md-3">
			<div class="panel panel-default">
				<div class="panel-heading"><strong>Agoonta</strong></div>
				<div class="panel-body" align="center">
					<h1><?php echo "5000"; ?></h1>
				</div>
			</div>
		</div>
		<?php
	}
	?>
</div>

<?php
include("footer.php");
?>