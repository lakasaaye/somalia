<div>
	<h1>Muqdisho: Maxkamadda ciidanka oo xukun ku ridey nin ku eedeysnaa inuu hubka u geyn jiray al-Shabaab</h1>
	<p>
		Arbaco, May, 08, 2019 (HOL) –  Maxkamadda darajada koowaad ee ciidanka qalabka sida ayaa maanta soo gabagabeeysay dacwad muddo dheer socotay oo la xiriirtay nin ku eedeysnaa inuu hubka u iibgeyn jiray ururka al-Shabaab.
		Eedeysanaha Cadow Cabdulle Axmed (Carab)   , ayaa maxkamadda laga sheegay inuu safarro ku kala bixiyay deegaannada ay ku suganyihiin al-Shabaab,halkaa oo la sheegay inuu hub geyn jiray.
		Guddoomiyaha Maxkamadda darajada koowaad ee ciidanka qalabka ,Gaashaanle Sare Xasan Cali Nuur Shuute ayaa sheegay eedeysane Cadow Cabdulle Axmed (Carab)   in lagu xukumay siddeed sanno oo xabsi ciidan ah.
		Xukunkan ayaa la sheegay inuu racfaan leeyahay, haddii uusan eedeysanaha ku qancin xukunka maxkamadda.

		Maxkamadda ayaa sidoo kale ka  digtay iibinta hubka qarsoodiga ah (Cir-toogte), waxa ayna maxkamadda fartay hay’adaha ammaanka in ay baaris iyo daba gal ku sameeyeen shaqsiyaadka ka ganacsada hubka.
	</p>
</div>