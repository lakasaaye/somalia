<?php
//header.php
?>
<!DOCTYPE html>
<html>
	<head>
		<title>Somali Military Management System</title>
		<script src="js/jquery-1.10.2.min.js"></script>
		<link rel="stylesheet" href="css/bootstrap.min.css" />
		<link rel="stylesheet" href="css/backend.css" />
		<script src="js/jquery.dataTables.min.js"></script>
		<script src="js/dataTables.bootstrap.min.js"></script>		
		<link rel="stylesheet" href="css/dataTables.bootstrap.min.css" />
		<script src="js/bootstrap.min.js"></script>
	</head>
	<body>
		<br />
		<div class="container">
			<h2 align="center">Somali Military Management System</h2>
			<nav class="navbar navbar-inverse">
			<a href="index.php"><img src="images/Somali_Army_Flag.png"></a>
				<div class="container-fluid">
					<div class="navbar-header">
						<a href="index.php" class="navbar-brand">Home</a>
					</div>
					<ul class="nav navbar-nav">
					<?php
					if($_SESSION['type'] == 'master')
					{
					?>
						<li><a href="user.php">Users</a></li>
						<li>
							<ul class="nav navbar-nav navbar-right">
								<li class="dropdown">
									<a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="label label-pill label-danger count"></span> Hoggaanada</a>
									<ul class="dropdown-menu">
										<li><a href="hohgelinta.php">H Howl Gelinta</a></li>
										<li><a href="hsahanka.php">H Sahanka</a></li>
										<li><a href="hoisgaarsiinta.php">H Isgaarsiinta</a></li>
										<li><a href="hhiyorasaasta.php">H Hubka Iyo Rasaasta</a></li>
										<li><a href="hsaadka.php">H Saadka</a></li>
										<li><a href="htababarka.php">H Tababarka</a></li>
									</ul>
								</li>
							</ul>
						</li>
						<li>
							<ul class="nav navbar-nav navbar-right">
								<li class="dropdown">
									<a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="label label-pill label-danger count"></span> Qaybaha</a>
									<ul class="dropdown-menu">
										<li><a href="q43cxds.php">Qaybta 43 CXDS</a></li>
										<li><a href="qlcxds.php">Qaybta 6 CXDS</a></li>
										<li><a href="qltacxds.php">Qaybta 12 April CXDS</a></li>
										<li><a href="qltcxds.php">Qaybta 27 CXDS</a></li>
										<li><a href="qklcxds.php">Qaybta 21 CXDS</a></li>
										<li><a href="qkacxds.php">Qaybta 54 CXDS</a></li>
									</ul>
								</li>
							</ul>
						</li>
						<li>
							<ul class="nav navbar-nav navbar-right">
								<li class="dropdown">
									<a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="label label-pill label-danger count"></span> Xafiisyada</a>
									<ul class="dropdown-menu">
										<li><a href="ururkalixdanaad.php">Xafiiska Ururka 60 Madaxtooyada</a></li>
										<li><a href="firepolice.php">Xafiiska Ururka 37 Fire Police</a></li>
										<li><a href="xcmbanaan.php">Xafiiska Colalka Madaxa Banaan</a></li>
									</ul>
								</li>
							</ul>
						</li>
						<li>
							<ul class="nav navbar-nav navbar-right">
								<li class="dropdown">
									<a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="label label-pill label-danger count"></span> Talisyada</a>
									<ul class="dropdown-menu">
										<li><a href="tcdhulka.php">Taliyaha Ciidanka Dhulka</a></li>
										<li><a href="tcbadda.php">Taliyaha Ciidanka Badda</a></li>
										<li><a href="tccirka.php">Taliyaha Ciidanka Cirka</a></li>
									</ul>
								</li>
							</ul>
						</li>
						<li>
							<ul class="nav navbar-nav navbar-right">
								<li class="dropdown">
									<a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="label label-pill label-danger count"></span> Military Life</a>
									<ul class="dropdown-menu">
										<li><a href="naafo.php">Naafada</a></li>
										<li><a href="agoon.php">Agoonta</a></li>
										<li><a href="darajo.php">Dalacsiinta</a></li>
									</ul>
								</li>
							</ul>
						</li>
					<?php
					}
					?>
						<li><a href="about.php">About</a></li>
						<li><a href="contact.php">Contact Us</a></li>
					</ul>
					<ul class="nav navbar-nav navbar-right">
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="label label-pill label-danger count"></span> <?php echo $_SESSION["user_name"]; ?></a>
							<ul class="dropdown-menu">
								<li><a href="profile.php">Profile</a></li>
								<li><a href="logout.php">Logout</a></li>
							</ul>
						</li>
					</ul>

				</div>
			</nav>