<?php
//brand.php

include('functionDashboard.php');

if(!isset($_SESSION['type']))
{
	header('location:login.php');
}

if($_SESSION['type'] != 'master')
{
	header('location:index.php');
}

include('header.php');

?>

	<span id="alert_action"></span>
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
                <div class="panel-heading">
                	<div class="row">
                		<div class="col-md-10">
                			<h3 class="panel-title">List-ga Qaybaha</h3>
                		</div>
                		<div class="col-md-2" align="right">
                			<button type="button" name="add" id="add_button" class="btn btn-success btn-xs">Add</button>
                		</div>
                	</div>
                </div>
                <div class="panel-body">
                	<table id="qayb_data" class="table table-bordered table-striped">
                		<thead>
							<tr>
								<th>ID</th>
								<th>Talis</th>
								<th>Name</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
						<?php
							$res=getQeybaha();
							if (!empty($res)) {
								while ($row=$res->fetch()) {
								echo '
								<tr>
									<td>'.$row["id"].'</td>
									<td>'.$row["qayb"].'</td>
									<td>'.$row["talis"].'</td>
									<td>
								<a class="btn btn-info" href="#" onclick="tusModel('.$row["id"].')"><i class="fa fa-eye"></i> Update</a>
								<a class="btn btn-danger" href="#" onclick="RemoveIncome('.$row["id"].')"><i class="fa fa-trash"></i> Delete</a>
								</td>
								</tr>
								';	
								}
							}
						 ?>
						 </tbody>
                	</table>
                </div>
            </div>
        </div>
    </div>

    <div id="qaybModal" class="modal fade">
    	<div class="modal-dialog">
    		<form method="post" id="qayb_form">
    			<div class="modal-content">
    				<div class="modal-header">
    					<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title"><i class="fa fa-plus"></i> Add Qayb</h4>
    				</div>
    				<div class="modal-body">
    					<div class="form-group">
    						<select name="talis_name" id="talis_name" class="form-control" required>
								<option value="">Select Talis</option>
								<?php //echo fill_talis_list($connect); ?>
							</select>
    					</div>
    					<div class="form-group">
							<label>Enter Qayb Name</label>
							<input type="text" name="qayb_name" id="qayb_name" class="form-control" required />
						</div>
    				</div>
    				<div class="modal-footer">
    					<input type="hidden" name="id" id="id" />
    					<input type="hidden" name="btn_action" id="btn_action" />
    					<input type="submit" name="action" id="action" class="btn btn-info" value="Add" />
    					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    				</div>
    			</div>
    		</form>
    	</div>
    </div>

<?php
include('footer.php');
?>
<script>
function tusModel(id) {
	// alert(id);
	$('#qaybModal').modal('show');
}

</script>








