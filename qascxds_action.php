<?php

//brand_action.php

include('database_connection.php');

if(isset($_POST['btn_action']))
{
	if($_POST['btn_action'] == 'Add')
	{
		$query = "
		INSERT INTO qaybaha (talis_id, name) 
		VALUES (:talis_id, :name)
		";
		$statement = $connect->prepare($query);
		$statement->execute(
			array(
				':talis_id'	=>	$_POST["talis_id"],
				':name'	=>	$_POST["name"]
			)
		);
		$result = $statement->fetchAll();
		if(isset($result))
		{
			echo 'Qayb Name Added';
		}
	}

	if($_POST['btn_action'] == 'fetch_single')
	{
		$query = "
		SELECT * FROM qaybaha WHERE id = :id
		";
		$statement = $connect->prepare($query);
		$statement->execute(
			array(
				':id'	=>	$_POST["id"]
			)
		);
		$result = $statement->fetchAll();
		foreach($result as $row)
		{
			$output['talis_id'] = $row['talis_id'];
			$output['name'] = $row['name'];
		}
		echo json_encode($output);
	}
	if($_POST['btn_action'] == 'Edit')
	{
		$query = "
		UPDATE qaybaha set 
		talis_id = :talis_id, 
		name = :name 
		WHERE id = :id
		";
		$statement = $connect->prepare($query);
		$statement->execute(
			array(
				':talis_id'	=>	$_POST["talis_id"],
				':name'	=>	$_POST["name"],
				':id'		=>	$_POST["id"]
			)
		);
		$result = $statement->fetchAll();
		if(isset($result))
		{
			echo 'Qayb Name Edited';
		}
	}

	if($_POST['btn_action'] == 'delete')
	{
		$query = "
		DELETE FROM qaybaha 
		WHERE id = :id
		";
		$statement = $connect->prepare($query);
		$statement->execute(
			array(
				':id'		=>	$_POST["id"]
			)
		);
		$result = $statement->fetchAll();
		if(isset($result))
		{
			echo 'Qaybtani waa la delete-day ' . $_POST["id"];
		}
	}
}

?>